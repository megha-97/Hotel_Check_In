**NEXT ASO - Evaluation Round-1**

PART-1 : 

Question: Write a regex to extract all the numbers with orange color background from the below text in italics.


{"orders":[{"id":_1_},{"id":_2_},{"id":_3_},{"id":_4_},{"id":_5_},{"id":_6_},{"id":_7_},{"id":_8_},{"id":_9_},{"id":_10_},{"id":_11_},{"id":_648_},{"id":_649_},{"id":_650_},{"id":_651_},{"id":_652_},{"id":_653_}],"errors":[{"code":_3_,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

Solution: I have used regular expressions to extract the numbers in italics. The function findall() is used for pattern matching. The condition included the colon ':' also as it helped to get rid of last few numbers which are not expected to be fetched.

PART-2 :

Problem Statement : To predict the customer who is going to check-in into the hotel using the history of customer booking in a hotel.

Approach: The process followed to solve this problem is as follows:
1. Read the train dataset provided and prepare the data by handling missing values.
2. Encode the categorical features using label encoding or one-hot encoding.
3. Trained Logistic regression and XGBoost model on train dataset.
4. Measured the accuracy on test dataset.
5. Found that only 3 features were contributing to the model predictions namely 'LodgingRevenue', 'RoomNights' and 'DaysSinceFirstStay'.
6. Trained another model using these 3 features only on dumped it into pickle file.
7. Created an API using streamlit.
8. Deployed the API on heroku.

Deployed URL Link : https://customer-check-in.herokuapp.com/


